package com.travlx.server.controller;

import com.travlx.server.sro.FeedNearMeRequest;
import com.travlx.server.sro.ProfileSummary;
import com.travlx.server.sro.TripSummary;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ditesh on 14/4/16.
 */
@RestController
@RequestMapping("/feed")
public class FeedController {

  @ApiOperation(value = "getFeedNearMe", nickname = "getFeedNearMe")
  @RequestMapping(method = RequestMethod.POST, path="/getFeedNearMe", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "user secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure - User OTP already verified")})
  public @ResponseBody List<TripSummary> getFeedNearMe (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestBody FeedNearMeRequest request){
    TripSummary ts = new TripSummary(1l, "http://dummy",0L,0L,new ProfileSummary("userid","","",""),"Dummy Trip");
    return Arrays.asList(ts);
  }

  @ApiOperation(value = "getMyFeedMaxOffset", nickname = "getMyFeedMaxOffset")
  @RequestMapping(method = RequestMethod.GET, path="/getMyFeedMaxOffset", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "user secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = Long.class),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public Long getMyFeedMaxOffset (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken){
    return 0l;
  }


  @ApiOperation(value = "getMyFeed", nickname = "getMyFeed")
  @RequestMapping(method = RequestMethod.GET, path="/getMyFeed", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "user secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439"),
      @ApiImplicitParam(name = "startOffset", value = "page start offset", required = true, dataType = "long", paramType = "query", defaultValue="0"),
      @ApiImplicitParam(name = "endOffset", value = "page end offset", required = true, dataType = "long", paramType = "query", defaultValue="5")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure - User OTP already verified")})
  public @ResponseBody List<TripSummary> getMyFeed (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestParam(value="startOffset") Long startOffset, @RequestParam(value="endOffset") Long endOffset){
    TripSummary ts = new TripSummary(1l, "http://dummy",0L,0L,new ProfileSummary("userid","","",""),"Dummy Trip");
    return Arrays.asList(ts);
  }
}
