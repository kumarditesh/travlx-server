package com.travlx.server.controller;

import com.travlx.server.sro.CheckinSummary;
import com.travlx.server.sro.ExploreCheckinsRequest;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ditesh on 16/4/16.
 */
@RestController
@RequestMapping("/explore")
public class ExploreController {

  @ApiOperation(value = "getExploreTagList", nickname = "getExploreTagList")
  @RequestMapping(method = RequestMethod.GET, path="/getExploreTagList", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "user secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public @ResponseBody List<String> getExploreTagList(@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken){
    return Arrays.asList("OutDoors","Food","Romantic","Adventure");
  }

  @ApiOperation(value = "exploreWithFilter", nickname = "exploreWithFilter")
  @RequestMapping(method = RequestMethod.POST, path="/exploreWithFilter", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "user secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure - User OTP already verified")})
  public @ResponseBody List<CheckinSummary> exploreWithFilter(@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestBody ExploreCheckinsRequest request){
    return Arrays.asList(new CheckinSummary(1l,"http://dummy", null));
  }
}
