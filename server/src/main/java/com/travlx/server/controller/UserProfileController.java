package com.travlx.server.controller;

import com.travlx.server.sro.*;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ditesh on 16/4/16.
 */
@RestController
@RequestMapping("/profile")
public class UserProfileController {

  @ApiOperation(value = "getUserProfile", nickname = "getUserProfile")
  @RequestMapping(method = RequestMethod.GET, path="/getUserProfile", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "caller user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "caller user's secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439"),
      @ApiImplicitParam(name = "requestedUserId", value = "user Id to be fetched", required = true, dataType = "string", paramType = "query", defaultValue="user-test-2")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public @ResponseBody UserProfile getUserProfile(@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestParam(value="requestedUserId") String requestedUserId){
    return new UserProfile("Dummy",0l,0l,"http://dummy");
  }

  @ApiOperation(value = "getUserPhotos", nickname = "getUserPhotos")
  @RequestMapping(method = RequestMethod.GET, path="/getUserPhotos", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "caller user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "caller user's secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439"),
      @ApiImplicitParam(name = "requestedUserId", value = "user Id to be fetched", required = true, dataType = "string", paramType = "query", defaultValue="user-test-2"),
      @ApiImplicitParam(name = "startOffset", value = "page start offset", required = true, dataType = "long", paramType = "query", defaultValue="0"),
      @ApiImplicitParam(name = "endOffset", value = "page end offset", required = true, dataType = "long", paramType = "query", defaultValue="5")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public @ResponseBody List<PhotoDetail> getUserPhotos (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestParam(value="requestedUserId") String requestedUserId,@RequestParam(value="startOffset") Long startOffset, @RequestParam(value="endOffset") Long endOffset) {
    return Arrays.asList(new PhotoDetail(1l,"http://dummy"));
  }

  @ApiOperation(value = "getUserCheckins", nickname = "getUserCheckins")
  @RequestMapping(method = RequestMethod.GET, path="/getUserCheckins", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "caller user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "caller user's secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439"),
      @ApiImplicitParam(name = "requestedUserId", value = "user Id to be fetched", required = true, dataType = "string", paramType = "query", defaultValue="user-test-2"),
      @ApiImplicitParam(name = "startOffset", value = "page start offset", required = true, dataType = "long", paramType = "query", defaultValue="0"),
      @ApiImplicitParam(name = "endOffset", value = "page end offset", required = true, dataType = "long", paramType = "query", defaultValue="5")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public @ResponseBody List<CheckinDetail> getUserCheckins (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestParam(value="requestedUserId") String requestedUserId,@RequestParam(value="startOffset") Long startOffset, @RequestParam(value="endOffset") Long endOffset) {
    return Arrays.asList(new CheckinDetail(1l,"http://dummy"));
  }


  @ApiOperation(value = "getUserTrips", nickname = "getUserTrips")
  @RequestMapping(method = RequestMethod.GET, path="/getUserTrips", produces = "application/json")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "userId", value = "caller user Id", required = true, dataType = "string", paramType = "query", defaultValue="user-test"),
      @ApiImplicitParam(name = "userToken", value = "caller user's secret token", required = true, dataType = "string", paramType = "query", defaultValue="dummy-password-57439"),
      @ApiImplicitParam(name = "requestedUserId", value = "user Id to be fetched", required = true, dataType = "string", paramType = "query", defaultValue="user-test-2"),
      @ApiImplicitParam(name = "startOffset", value = "page start offset", required = true, dataType = "long", paramType = "query", defaultValue="0"),
      @ApiImplicitParam(name = "endOffset", value = "page end offset", required = true, dataType = "long", paramType = "query", defaultValue="5")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  public @ResponseBody List<TripSummary> getUserTrips (@RequestParam(value="userId") String userId, @RequestParam(value="userToken") String userToken,
      @RequestParam(value="requestedUserId") String requestedUserId,@RequestParam(value="startOffset") Long startOffset, @RequestParam(value="endOffset") Long endOffset) {
    TripSummary ts = new TripSummary(1l, "http://dummy",0L,0L,null,"Dummy Trip");
    return Arrays.asList(ts);
  }
}
