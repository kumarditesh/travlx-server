package com.travlx.server.sro;

/**
 * Created by ditesh on 15/4/16.
 */
public class OtpVerified {
  
  private Boolean isVerified;
  private String userPass;

  public OtpVerified(Boolean isVerified, String userPass) {
    this.isVerified = isVerified;
    this.userPass = userPass;
  }

  public Boolean getIsVerified() {
    return isVerified;
  }

  public void setIsVerified(Boolean isVerified) {
    this.isVerified = isVerified;
  }

  public String getUserPass() {
    return userPass;
  }

  public void setUserPass(String userPass) {
    this.userPass = userPass;
  }
}
