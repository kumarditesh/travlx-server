package com.travlx.server.sro;

/**
 * Created by ditesh on 15/4/16.
 */
public class UserProfileCreateRequest {
  private String name;
  private String handle;
  private String gender;
  private String dpUrl;
  private Location loc;

  public UserProfileCreateRequest() {
  }

  public UserProfileCreateRequest(String name, String handle, String gender, String dpUrl, Location loc) {
    this.name = name;
    this.handle = handle;
    this.gender = gender;
    this.dpUrl = dpUrl;
    this.loc = loc;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getHandle() {
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getDpUrl() {
    return dpUrl;
  }

  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }

  public Location getLoc() {
    return loc;
  }

  public void setLoc(Location loc) {
    this.loc = loc;
  }
}
