package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class FeedNearMeRequest {

  private Long startOffset; 
  private Long endOffset;
  private Location local;

  public FeedNearMeRequest() {
  }

  public FeedNearMeRequest(Long startOffset, Long endOffset, Location local) {
    this.startOffset = startOffset;
    this.endOffset = endOffset;
    this.local = local;
  }

  public Long getStartOffset() {
    return startOffset;
  }

  public void setStartOffset(Long startOffset) {
    this.startOffset = startOffset;
  }

  public Long getEndOffset() {
    return endOffset;
  }

  public void setEndOffset(Long endOffset) {
    this.endOffset = endOffset;
  }

  public Location getLocal() {
    return local;
  }

  public void setLocal(Location local) {
    this.local = local;
  }
}
