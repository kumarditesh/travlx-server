package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class UserProfile {
  
  private String name;
  private Long friendsCount;
  private Long followingsCount;
  private String dpUrl;

  public UserProfile() {
  }

  public UserProfile(String name, Long friendsCount, Long followingsCount, String dpUrl) {
    this.name = name;
    this.friendsCount = friendsCount;
    this.followingsCount = followingsCount;
    this.dpUrl = dpUrl;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getFriendsCount() {
    return friendsCount;
  }

  public void setFriendsCount(Long friendsCount) {
    this.friendsCount = friendsCount;
  }

  public Long getFollowingsCount() {
    return followingsCount;
  }

  public void setFollowingsCount(Long followingsCount) {
    this.followingsCount = followingsCount;
  }

  public String getDpUrl() {
    return dpUrl;
  }

  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }
}
