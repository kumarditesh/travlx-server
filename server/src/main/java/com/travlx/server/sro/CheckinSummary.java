package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class CheckinSummary {
  
  private Long checkinId;
  private String coverPicUrl;
  private Location location;

  public CheckinSummary() {
  }

  public CheckinSummary(Long checkinId, String coverPicUrl, Location location) {
    this.checkinId = checkinId;
    this.coverPicUrl = coverPicUrl;
    this.location = location;
  }

  public Long getCheckinId() {
    return checkinId;
  }

  public void setCheckinId(Long checkinId) {
    this.checkinId = checkinId;
  }

  public String getCoverPicUrl() {
    return coverPicUrl;
  }

  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }
}
