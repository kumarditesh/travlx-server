package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class ProfileSummary {
  
  private String userId;
  private String handle;
  private String name;
  private String dpUrl;

  public ProfileSummary(String userId, String handle, String name, String dpUrl) {
    this.userId = userId;
    this.handle = handle;
    this.name = name;
    this.dpUrl = dpUrl;
  }

  public ProfileSummary() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getHandle() {
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDpUrl() {
    return dpUrl;
  }

  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }
}
