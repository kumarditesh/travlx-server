package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class PhotoDetail {
  
  private Long photoId;
  private String photoUrl;

  public PhotoDetail() {
  }

  public PhotoDetail(Long photoId, String photoUrl) {
    this.photoId = photoId;
    this.photoUrl = photoUrl;
  }

  public Long getPhotoId() {
    return photoId;
  }

  public void setPhotoId(Long photoId) {
    this.photoId = photoId;
  }

  public String getPhotoUrl() {
    return photoUrl;
  }

  public void setPhotoUrl(String photoUrl) {
    this.photoUrl = photoUrl;
  }
}
