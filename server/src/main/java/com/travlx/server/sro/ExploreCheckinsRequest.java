package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class ExploreCheckinsRequest {

  private Long startOffset;
  private Long endOffset;
  private String filter;
  private Location location;

  public ExploreCheckinsRequest() {
  }

  public ExploreCheckinsRequest(Long startOffset, Long endOffset, String filter, Location location) {
    this.startOffset = startOffset;
    this.endOffset = endOffset;
    this.filter = filter;
    this.location = location;
  }

  public Long getStartOffset() {
    return startOffset;
  }

  public void setStartOffset(Long startOffset) {
    this.startOffset = startOffset;
  }

  public Long getEndOffset() {
    return endOffset;
  }

  public void setEndOffset(Long endOffset) {
    this.endOffset = endOffset;
  }

  public String getFilter() {
    return filter;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }
}
