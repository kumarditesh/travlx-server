package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class CheckinDetail {
  
  private Long id;
  private String coverPicUrl;

  public CheckinDetail() {
  }

  public CheckinDetail(Long id, String coverPicUrl) {
  
    this.id = id;
    this.coverPicUrl = coverPicUrl;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCoverPicUrl() {
    return coverPicUrl;
  }

  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }
}
