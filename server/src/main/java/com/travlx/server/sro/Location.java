package com.travlx.server.sro;

/**
 * Created by ditesh on 15/4/16.
 */
public class Location {
  
  private Double latitude;
  private Double longitude;
  private Double altitude;
  private Double horizontalAccuracy;
  private Double verticalAccuracy;

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getAltitude() {
    return altitude;
  }

  public void setAltitude(Double altitude) {
    this.altitude = altitude;
  }

  public Double getHorizontalAccuracy() {
    return horizontalAccuracy;
  }

  public void setHorizontalAccuracy(Double horizontalAccuracy) {
    this.horizontalAccuracy = horizontalAccuracy;
  }

  public Double getVerticalAccuracy() {
    return verticalAccuracy;
  }

  public void setVerticalAccuracy(Double verticalAccuracy) {
    this.verticalAccuracy = verticalAccuracy;
  }

  public Location(Double latitude, Double longitude, Double altitude, Double horizontalAccuracy, Double verticalAccuracy) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
    this.horizontalAccuracy = horizontalAccuracy;
    this.verticalAccuracy = verticalAccuracy;
  }

  public Location() {
  }
}
