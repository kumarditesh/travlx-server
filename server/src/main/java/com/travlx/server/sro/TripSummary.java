package com.travlx.server.sro;

/**
 * Created by ditesh on 16/4/16.
 */
public class TripSummary {
  
  private Long tripId;
  private String coverPicUrl;
  private Long bookmarkCount;
  private Long likeCount;
  private ProfileSummary owner;
  private String title;

  public TripSummary() {
  }

  public TripSummary(Long tripId, String coverPicUrl, Long bookmarkCount, Long likeCount, ProfileSummary owner, String title) {
    this.tripId = tripId;
    this.coverPicUrl = coverPicUrl;
    this.bookmarkCount = bookmarkCount;
    this.likeCount = likeCount;
    this.owner = owner;
    this.title = title;
  }

  public String getCoverPicUrl() {
    return coverPicUrl;
  }

  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }

  public Long getBookmarkCount() {
    return bookmarkCount;
  }

  public void setBookmarkCount(Long bookmarkCount) {
    this.bookmarkCount = bookmarkCount;
  }

  public Long getLikeCount() {
    return likeCount;
  }

  public void setLikeCount(Long likeCount) {
    this.likeCount = likeCount;
  }

  public ProfileSummary getOwner() {
    return owner;
  }

  public void setOwner(ProfileSummary owner) {
    this.owner = owner;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getTripId() {
    return tripId;
  }

  public void setTripId(Long tripId) {
    this.tripId = tripId;
  }
}
