package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:16.287+05:30")
public class Location   {
  
  private Double altitude = null;
  private Double horizontalAccuracy = null;
  private Double latitude = null;
  private Double longitude = null;
  private Double verticalAccuracy = null;

  
  /**
   **/
  public Location altitude(Double altitude) {
    this.altitude = altitude;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("altitude")
  public Double getAltitude() {
    return altitude;
  }
  public void setAltitude(Double altitude) {
    this.altitude = altitude;
  }

  
  /**
   **/
  public Location horizontalAccuracy(Double horizontalAccuracy) {
    this.horizontalAccuracy = horizontalAccuracy;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("horizontalAccuracy")
  public Double getHorizontalAccuracy() {
    return horizontalAccuracy;
  }
  public void setHorizontalAccuracy(Double horizontalAccuracy) {
    this.horizontalAccuracy = horizontalAccuracy;
  }

  
  /**
   **/
  public Location latitude(Double latitude) {
    this.latitude = latitude;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("latitude")
  public Double getLatitude() {
    return latitude;
  }
  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  
  /**
   **/
  public Location longitude(Double longitude) {
    this.longitude = longitude;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("longitude")
  public Double getLongitude() {
    return longitude;
  }
  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  
  /**
   **/
  public Location verticalAccuracy(Double verticalAccuracy) {
    this.verticalAccuracy = verticalAccuracy;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("verticalAccuracy")
  public Double getVerticalAccuracy() {
    return verticalAccuracy;
  }
  public void setVerticalAccuracy(Double verticalAccuracy) {
    this.verticalAccuracy = verticalAccuracy;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Location location = (Location) o;
    return Objects.equals(this.altitude, location.altitude) &&
        Objects.equals(this.horizontalAccuracy, location.horizontalAccuracy) &&
        Objects.equals(this.latitude, location.latitude) &&
        Objects.equals(this.longitude, location.longitude) &&
        Objects.equals(this.verticalAccuracy, location.verticalAccuracy);
  }

  @Override
  public int hashCode() {
    return Objects.hash(altitude, horizontalAccuracy, latitude, longitude, verticalAccuracy);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Location {\n");
    
    sb.append("    altitude: ").append(toIndentedString(altitude)).append("\n");
    sb.append("    horizontalAccuracy: ").append(toIndentedString(horizontalAccuracy)).append("\n");
    sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
    sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
    sb.append("    verticalAccuracy: ").append(toIndentedString(verticalAccuracy)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

