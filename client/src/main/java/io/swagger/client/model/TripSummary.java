package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.ProfileSummary;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class TripSummary   {
  
  private Long bookmarkCount = null;
  private String coverPicUrl = null;
  private Long likeCount = null;
  private ProfileSummary owner = null;
  private String title = null;
  private Long tripId = null;

  
  /**
   **/
  public TripSummary bookmarkCount(Long bookmarkCount) {
    this.bookmarkCount = bookmarkCount;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("bookmarkCount")
  public Long getBookmarkCount() {
    return bookmarkCount;
  }
  public void setBookmarkCount(Long bookmarkCount) {
    this.bookmarkCount = bookmarkCount;
  }

  
  /**
   **/
  public TripSummary coverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("coverPicUrl")
  public String getCoverPicUrl() {
    return coverPicUrl;
  }
  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }

  
  /**
   **/
  public TripSummary likeCount(Long likeCount) {
    this.likeCount = likeCount;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("likeCount")
  public Long getLikeCount() {
    return likeCount;
  }
  public void setLikeCount(Long likeCount) {
    this.likeCount = likeCount;
  }

  
  /**
   **/
  public TripSummary owner(ProfileSummary owner) {
    this.owner = owner;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("owner")
  public ProfileSummary getOwner() {
    return owner;
  }
  public void setOwner(ProfileSummary owner) {
    this.owner = owner;
  }

  
  /**
   **/
  public TripSummary title(String title) {
    this.title = title;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  
  /**
   **/
  public TripSummary tripId(Long tripId) {
    this.tripId = tripId;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("tripId")
  public Long getTripId() {
    return tripId;
  }
  public void setTripId(Long tripId) {
    this.tripId = tripId;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TripSummary tripSummary = (TripSummary) o;
    return Objects.equals(this.bookmarkCount, tripSummary.bookmarkCount) &&
        Objects.equals(this.coverPicUrl, tripSummary.coverPicUrl) &&
        Objects.equals(this.likeCount, tripSummary.likeCount) &&
        Objects.equals(this.owner, tripSummary.owner) &&
        Objects.equals(this.title, tripSummary.title) &&
        Objects.equals(this.tripId, tripSummary.tripId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bookmarkCount, coverPicUrl, likeCount, owner, title, tripId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TripSummary {\n");
    
    sb.append("    bookmarkCount: ").append(toIndentedString(bookmarkCount)).append("\n");
    sb.append("    coverPicUrl: ").append(toIndentedString(coverPicUrl)).append("\n");
    sb.append("    likeCount: ").append(toIndentedString(likeCount)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    tripId: ").append(toIndentedString(tripId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

