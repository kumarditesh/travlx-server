package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Location;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:15.541+05:30")
public class UserProfileCreateRequest   {
  
  private String dpUrl = null;
  private String gender = null;
  private String handle = null;
  private Location loc = null;
  private String name = null;

  
  /**
   **/
  public UserProfileCreateRequest dpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("dpUrl")
  public String getDpUrl() {
    return dpUrl;
  }
  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }

  
  /**
   **/
  public UserProfileCreateRequest gender(String gender) {
    this.gender = gender;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("gender")
  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }

  
  /**
   **/
  public UserProfileCreateRequest handle(String handle) {
    this.handle = handle;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("handle")
  public String getHandle() {
    return handle;
  }
  public void setHandle(String handle) {
    this.handle = handle;
  }

  
  /**
   **/
  public UserProfileCreateRequest loc(Location loc) {
    this.loc = loc;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("loc")
  public Location getLoc() {
    return loc;
  }
  public void setLoc(Location loc) {
    this.loc = loc;
  }

  
  /**
   **/
  public UserProfileCreateRequest name(String name) {
    this.name = name;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserProfileCreateRequest userProfileCreateRequest = (UserProfileCreateRequest) o;
    return Objects.equals(this.dpUrl, userProfileCreateRequest.dpUrl) &&
        Objects.equals(this.gender, userProfileCreateRequest.gender) &&
        Objects.equals(this.handle, userProfileCreateRequest.handle) &&
        Objects.equals(this.loc, userProfileCreateRequest.loc) &&
        Objects.equals(this.name, userProfileCreateRequest.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dpUrl, gender, handle, loc, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserProfileCreateRequest {\n");
    
    sb.append("    dpUrl: ").append(toIndentedString(dpUrl)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    handle: ").append(toIndentedString(handle)).append("\n");
    sb.append("    loc: ").append(toIndentedString(loc)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

