package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class UserProfile   {
  
  private String dpUrl = null;
  private Long followingsCount = null;
  private Long friendsCount = null;
  private String name = null;

  
  /**
   **/
  public UserProfile dpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("dpUrl")
  public String getDpUrl() {
    return dpUrl;
  }
  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }

  
  /**
   **/
  public UserProfile followingsCount(Long followingsCount) {
    this.followingsCount = followingsCount;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("followingsCount")
  public Long getFollowingsCount() {
    return followingsCount;
  }
  public void setFollowingsCount(Long followingsCount) {
    this.followingsCount = followingsCount;
  }

  
  /**
   **/
  public UserProfile friendsCount(Long friendsCount) {
    this.friendsCount = friendsCount;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("friendsCount")
  public Long getFriendsCount() {
    return friendsCount;
  }
  public void setFriendsCount(Long friendsCount) {
    this.friendsCount = friendsCount;
  }

  
  /**
   **/
  public UserProfile name(String name) {
    this.name = name;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserProfile userProfile = (UserProfile) o;
    return Objects.equals(this.dpUrl, userProfile.dpUrl) &&
        Objects.equals(this.followingsCount, userProfile.followingsCount) &&
        Objects.equals(this.friendsCount, userProfile.friendsCount) &&
        Objects.equals(this.name, userProfile.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dpUrl, followingsCount, friendsCount, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserProfile {\n");
    
    sb.append("    dpUrl: ").append(toIndentedString(dpUrl)).append("\n");
    sb.append("    followingsCount: ").append(toIndentedString(followingsCount)).append("\n");
    sb.append("    friendsCount: ").append(toIndentedString(friendsCount)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

