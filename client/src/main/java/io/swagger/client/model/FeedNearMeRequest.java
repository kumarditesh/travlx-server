package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Location;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:13.757+05:30")
public class FeedNearMeRequest   {
  
  private Long endOffset = null;
  private Location local = null;
  private Long startOffset = null;

  
  /**
   **/
  public FeedNearMeRequest endOffset(Long endOffset) {
    this.endOffset = endOffset;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("endOffset")
  public Long getEndOffset() {
    return endOffset;
  }
  public void setEndOffset(Long endOffset) {
    this.endOffset = endOffset;
  }

  
  /**
   **/
  public FeedNearMeRequest local(Location local) {
    this.local = local;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("local")
  public Location getLocal() {
    return local;
  }
  public void setLocal(Location local) {
    this.local = local;
  }

  
  /**
   **/
  public FeedNearMeRequest startOffset(Long startOffset) {
    this.startOffset = startOffset;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("startOffset")
  public Long getStartOffset() {
    return startOffset;
  }
  public void setStartOffset(Long startOffset) {
    this.startOffset = startOffset;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FeedNearMeRequest feedNearMeRequest = (FeedNearMeRequest) o;
    return Objects.equals(this.endOffset, feedNearMeRequest.endOffset) &&
        Objects.equals(this.local, feedNearMeRequest.local) &&
        Objects.equals(this.startOffset, feedNearMeRequest.startOffset);
  }

  @Override
  public int hashCode() {
    return Objects.hash(endOffset, local, startOffset);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FeedNearMeRequest {\n");
    
    sb.append("    endOffset: ").append(toIndentedString(endOffset)).append("\n");
    sb.append("    local: ").append(toIndentedString(local)).append("\n");
    sb.append("    startOffset: ").append(toIndentedString(startOffset)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

