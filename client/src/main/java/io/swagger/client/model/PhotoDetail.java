package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class PhotoDetail   {
  
  private Long photoId = null;
  private String photoUrl = null;

  
  /**
   **/
  public PhotoDetail photoId(Long photoId) {
    this.photoId = photoId;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("photoId")
  public Long getPhotoId() {
    return photoId;
  }
  public void setPhotoId(Long photoId) {
    this.photoId = photoId;
  }

  
  /**
   **/
  public PhotoDetail photoUrl(String photoUrl) {
    this.photoUrl = photoUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("photoUrl")
  public String getPhotoUrl() {
    return photoUrl;
  }
  public void setPhotoUrl(String photoUrl) {
    this.photoUrl = photoUrl;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PhotoDetail photoDetail = (PhotoDetail) o;
    return Objects.equals(this.photoId, photoDetail.photoId) &&
        Objects.equals(this.photoUrl, photoDetail.photoUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(photoId, photoUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PhotoDetail {\n");
    
    sb.append("    photoId: ").append(toIndentedString(photoId)).append("\n");
    sb.append("    photoUrl: ").append(toIndentedString(photoUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

