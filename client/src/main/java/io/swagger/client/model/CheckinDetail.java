package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class CheckinDetail   {
  
  private String coverPicUrl = null;
  private Long id = null;

  
  /**
   **/
  public CheckinDetail coverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("coverPicUrl")
  public String getCoverPicUrl() {
    return coverPicUrl;
  }
  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }

  
  /**
   **/
  public CheckinDetail id(Long id) {
    this.id = id;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CheckinDetail checkinDetail = (CheckinDetail) o;
    return Objects.equals(this.coverPicUrl, checkinDetail.coverPicUrl) &&
        Objects.equals(this.id, checkinDetail.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(coverPicUrl, id);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CheckinDetail {\n");
    
    sb.append("    coverPicUrl: ").append(toIndentedString(coverPicUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

