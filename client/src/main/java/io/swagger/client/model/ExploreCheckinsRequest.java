package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Location;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:16.287+05:30")
public class ExploreCheckinsRequest   {
  
  private Long endOffset = null;
  private String filter = null;
  private Location location = null;
  private Long startOffset = null;

  
  /**
   **/
  public ExploreCheckinsRequest endOffset(Long endOffset) {
    this.endOffset = endOffset;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("endOffset")
  public Long getEndOffset() {
    return endOffset;
  }
  public void setEndOffset(Long endOffset) {
    this.endOffset = endOffset;
  }

  
  /**
   **/
  public ExploreCheckinsRequest filter(String filter) {
    this.filter = filter;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("filter")
  public String getFilter() {
    return filter;
  }
  public void setFilter(String filter) {
    this.filter = filter;
  }

  
  /**
   **/
  public ExploreCheckinsRequest location(Location location) {
    this.location = location;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("location")
  public Location getLocation() {
    return location;
  }
  public void setLocation(Location location) {
    this.location = location;
  }

  
  /**
   **/
  public ExploreCheckinsRequest startOffset(Long startOffset) {
    this.startOffset = startOffset;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("startOffset")
  public Long getStartOffset() {
    return startOffset;
  }
  public void setStartOffset(Long startOffset) {
    this.startOffset = startOffset;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExploreCheckinsRequest exploreCheckinsRequest = (ExploreCheckinsRequest) o;
    return Objects.equals(this.endOffset, exploreCheckinsRequest.endOffset) &&
        Objects.equals(this.filter, exploreCheckinsRequest.filter) &&
        Objects.equals(this.location, exploreCheckinsRequest.location) &&
        Objects.equals(this.startOffset, exploreCheckinsRequest.startOffset);
  }

  @Override
  public int hashCode() {
    return Objects.hash(endOffset, filter, location, startOffset);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExploreCheckinsRequest {\n");
    
    sb.append("    endOffset: ").append(toIndentedString(endOffset)).append("\n");
    sb.append("    filter: ").append(toIndentedString(filter)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    startOffset: ").append(toIndentedString(startOffset)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

