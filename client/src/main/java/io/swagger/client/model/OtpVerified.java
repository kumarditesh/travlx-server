package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:15.541+05:30")
public class OtpVerified   {
  
  private Boolean isVerified = null;
  private String userPass = null;

  
  /**
   **/
  public OtpVerified isVerified(Boolean isVerified) {
    this.isVerified = isVerified;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("isVerified")
  public Boolean getIsVerified() {
    return isVerified;
  }
  public void setIsVerified(Boolean isVerified) {
    this.isVerified = isVerified;
  }

  
  /**
   **/
  public OtpVerified userPass(String userPass) {
    this.userPass = userPass;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("userPass")
  public String getUserPass() {
    return userPass;
  }
  public void setUserPass(String userPass) {
    this.userPass = userPass;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OtpVerified otpVerified = (OtpVerified) o;
    return Objects.equals(this.isVerified, otpVerified.isVerified) &&
        Objects.equals(this.userPass, otpVerified.userPass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isVerified, userPass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OtpVerified {\n");
    
    sb.append("    isVerified: ").append(toIndentedString(isVerified)).append("\n");
    sb.append("    userPass: ").append(toIndentedString(userPass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

