package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class ProfileSummary   {
  
  private String dpUrl = null;
  private String handle = null;
  private String name = null;
  private String userId = null;

  
  /**
   **/
  public ProfileSummary dpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("dpUrl")
  public String getDpUrl() {
    return dpUrl;
  }
  public void setDpUrl(String dpUrl) {
    this.dpUrl = dpUrl;
  }

  
  /**
   **/
  public ProfileSummary handle(String handle) {
    this.handle = handle;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("handle")
  public String getHandle() {
    return handle;
  }
  public void setHandle(String handle) {
    this.handle = handle;
  }

  
  /**
   **/
  public ProfileSummary name(String name) {
    this.name = name;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   **/
  public ProfileSummary userId(String userId) {
    this.userId = userId;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("userId")
  public String getUserId() {
    return userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProfileSummary profileSummary = (ProfileSummary) o;
    return Objects.equals(this.dpUrl, profileSummary.dpUrl) &&
        Objects.equals(this.handle, profileSummary.handle) &&
        Objects.equals(this.name, profileSummary.name) &&
        Objects.equals(this.userId, profileSummary.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dpUrl, handle, name, userId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProfileSummary {\n");
    
    sb.append("    dpUrl: ").append(toIndentedString(dpUrl)).append("\n");
    sb.append("    handle: ").append(toIndentedString(handle)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

