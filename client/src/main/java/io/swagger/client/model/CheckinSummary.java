package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Location;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:16.287+05:30")
public class CheckinSummary   {
  
  private Long checkinId = null;
  private String coverPicUrl = null;
  private Location location = null;

  
  /**
   **/
  public CheckinSummary checkinId(Long checkinId) {
    this.checkinId = checkinId;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("checkinId")
  public Long getCheckinId() {
    return checkinId;
  }
  public void setCheckinId(Long checkinId) {
    this.checkinId = checkinId;
  }

  
  /**
   **/
  public CheckinSummary coverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("coverPicUrl")
  public String getCoverPicUrl() {
    return coverPicUrl;
  }
  public void setCoverPicUrl(String coverPicUrl) {
    this.coverPicUrl = coverPicUrl;
  }

  
  /**
   **/
  public CheckinSummary location(Location location) {
    this.location = location;
    return this;
  }
  
  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("location")
  public Location getLocation() {
    return location;
  }
  public void setLocation(Location location) {
    this.location = location;
  }

  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CheckinSummary checkinSummary = (CheckinSummary) o;
    return Objects.equals(this.checkinId, checkinSummary.checkinId) &&
        Objects.equals(this.coverPicUrl, checkinSummary.coverPicUrl) &&
        Objects.equals(this.location, checkinSummary.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(checkinId, coverPicUrl, location);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CheckinSummary {\n");
    
    sb.append("    checkinId: ").append(toIndentedString(checkinId)).append("\n");
    sb.append("    coverPicUrl: ").append(toIndentedString(coverPicUrl)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

