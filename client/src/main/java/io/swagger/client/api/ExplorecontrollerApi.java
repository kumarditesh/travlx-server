package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.ExploreCheckinsRequest;
import io.swagger.client.model.CheckinSummary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:16.287+05:30")
public class ExplorecontrollerApi {
  private ApiClient apiClient;

  public ExplorecontrollerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ExplorecontrollerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * exploreWithFilter
   * 
   * @param request request (required)
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @return List<CheckinSummary>
   * @throws ApiException if fails to make API call
   */
  public List<CheckinSummary> exploreWithFilter(ExploreCheckinsRequest request, String userId, String userToken) throws ApiException {
    Object localVarPostBody = request;
    
    // verify the required parameter 'request' is set
    if (request == null) {
      throw new ApiException(400, "Missing the required parameter 'request' when calling exploreWithFilter");
    }
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling exploreWithFilter");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling exploreWithFilter");
    }
    
    // create path and map variables
    String localVarPath = "/explore/exploreWithFilter".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<CheckinSummary>> localVarReturnType = new GenericType<List<CheckinSummary>>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getExploreTagList
   * 
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @return List<String>
   * @throws ApiException if fails to make API call
   */
  public List<String> getExploreTagList(String userId, String userToken) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getExploreTagList");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getExploreTagList");
    }
    
    // create path and map variables
    String localVarPath = "/explore/getExploreTagList".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<String>> localVarReturnType = new GenericType<List<String>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
}
