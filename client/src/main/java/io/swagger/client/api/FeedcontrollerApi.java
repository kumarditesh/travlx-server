package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.FeedNearMeRequest;
import io.swagger.client.model.TripSummary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:13.757+05:30")
public class FeedcontrollerApi {
  private ApiClient apiClient;

  public FeedcontrollerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public FeedcontrollerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * getFeedNearMe
   * 
   * @param request request (required)
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @return List<TripSummary>
   * @throws ApiException if fails to make API call
   */
  public List<TripSummary> getFeedNearMe(FeedNearMeRequest request, String userId, String userToken) throws ApiException {
    Object localVarPostBody = request;
    
    // verify the required parameter 'request' is set
    if (request == null) {
      throw new ApiException(400, "Missing the required parameter 'request' when calling getFeedNearMe");
    }
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getFeedNearMe");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getFeedNearMe");
    }
    
    // create path and map variables
    String localVarPath = "/feed/getFeedNearMe".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<TripSummary>> localVarReturnType = new GenericType<List<TripSummary>>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getMyFeed
   * 
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @param startOffset page start offset (required)
   * @param endOffset page end offset (required)
   * @return List<TripSummary>
   * @throws ApiException if fails to make API call
   */
  public List<TripSummary> getMyFeed(String userId, String userToken, Long startOffset, Long endOffset) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getMyFeed");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getMyFeed");
    }
    
    // verify the required parameter 'startOffset' is set
    if (startOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'startOffset' when calling getMyFeed");
    }
    
    // verify the required parameter 'endOffset' is set
    if (endOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'endOffset' when calling getMyFeed");
    }
    
    // create path and map variables
    String localVarPath = "/feed/getMyFeed".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "startOffset", startOffset));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "endOffset", endOffset));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<TripSummary>> localVarReturnType = new GenericType<List<TripSummary>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getMyFeedMaxOffset
   * 
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @return Long
   * @throws ApiException if fails to make API call
   */
  public Long getMyFeedMaxOffset(String userId, String userToken) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getMyFeedMaxOffset");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getMyFeedMaxOffset");
    }
    
    // create path and map variables
    String localVarPath = "/feed/getMyFeedMaxOffset".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<Long> localVarReturnType = new GenericType<Long>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
}
