package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.OtpVerified;
import io.swagger.client.model.UserProfileCreateRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:15.541+05:30")
public class LogincontrollerApi {
  private ApiClient apiClient;

  public LogincontrollerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public LogincontrollerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * createProfile
   * 
   * @param profile profile (required)
   * @param userId user Id (required)
   * @param userToken user secret token (required)
   * @return OtpVerified
   * @throws ApiException if fails to make API call
   */
  public OtpVerified createProfile(UserProfileCreateRequest profile, String userId, String userToken) throws ApiException {
    Object localVarPostBody = profile;
    
    // verify the required parameter 'profile' is set
    if (profile == null) {
      throw new ApiException(400, "Missing the required parameter 'profile' when calling createProfile");
    }
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling createProfile");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling createProfile");
    }
    
    // create path and map variables
    String localVarPath = "/login/createProfile".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<OtpVerified> localVarReturnType = new GenericType<OtpVerified>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getOtp
   * 
   * @param countryCode User phone number&#39;s country code (required)
   * @param phoneNumber User phone number (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getOtp(String countryCode, String phoneNumber) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'countryCode' is set
    if (countryCode == null) {
      throw new ApiException(400, "Missing the required parameter 'countryCode' when calling getOtp");
    }
    
    // verify the required parameter 'phoneNumber' is set
    if (phoneNumber == null) {
      throw new ApiException(400, "Missing the required parameter 'phoneNumber' when calling getOtp");
    }
    
    // create path and map variables
    String localVarPath = "/login/getOtp".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "countryCode", countryCode));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "phoneNumber", phoneNumber));
    

    

    

    final String[] localVarAccepts = {
      "text/plain"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<String> localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * verifyOtp
   * 
   * @param otp otp entered by user (required)
   * @param userId User ID generated by getOtp (required)
   * @return Boolean
   * @throws ApiException if fails to make API call
   */
  public Boolean verifyOtp(String otp, String userId) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'otp' is set
    if (otp == null) {
      throw new ApiException(400, "Missing the required parameter 'otp' when calling verifyOtp");
    }
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling verifyOtp");
    }
    
    // create path and map variables
    String localVarPath = "/login/verifyOtp".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "otp", otp));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<Boolean> localVarReturnType = new GenericType<Boolean>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
}
