package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.CheckinDetail;
import io.swagger.client.model.PhotoDetail;
import io.swagger.client.model.UserProfile;
import io.swagger.client.model.TripSummary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-04-16T17:50:14.630+05:30")
public class UserprofilecontrollerApi {
  private ApiClient apiClient;

  public UserprofilecontrollerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public UserprofilecontrollerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * getUserCheckins
   * 
   * @param userId caller user Id (required)
   * @param userToken caller user&#39;s secret token (required)
   * @param requestedUserId user Id to be fetched (required)
   * @param startOffset page start offset (required)
   * @param endOffset page end offset (required)
   * @return List<CheckinDetail>
   * @throws ApiException if fails to make API call
   */
  public List<CheckinDetail> getUserCheckins(String userId, String userToken, String requestedUserId, Long startOffset, Long endOffset) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getUserCheckins");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getUserCheckins");
    }
    
    // verify the required parameter 'requestedUserId' is set
    if (requestedUserId == null) {
      throw new ApiException(400, "Missing the required parameter 'requestedUserId' when calling getUserCheckins");
    }
    
    // verify the required parameter 'startOffset' is set
    if (startOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'startOffset' when calling getUserCheckins");
    }
    
    // verify the required parameter 'endOffset' is set
    if (endOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'endOffset' when calling getUserCheckins");
    }
    
    // create path and map variables
    String localVarPath = "/profile/getUserCheckins".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestedUserId", requestedUserId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "startOffset", startOffset));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "endOffset", endOffset));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<CheckinDetail>> localVarReturnType = new GenericType<List<CheckinDetail>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getUserPhotos
   * 
   * @param userId caller user Id (required)
   * @param userToken caller user&#39;s secret token (required)
   * @param requestedUserId user Id to be fetched (required)
   * @param startOffset page start offset (required)
   * @param endOffset page end offset (required)
   * @return List<PhotoDetail>
   * @throws ApiException if fails to make API call
   */
  public List<PhotoDetail> getUserPhotos(String userId, String userToken, String requestedUserId, Long startOffset, Long endOffset) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getUserPhotos");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getUserPhotos");
    }
    
    // verify the required parameter 'requestedUserId' is set
    if (requestedUserId == null) {
      throw new ApiException(400, "Missing the required parameter 'requestedUserId' when calling getUserPhotos");
    }
    
    // verify the required parameter 'startOffset' is set
    if (startOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'startOffset' when calling getUserPhotos");
    }
    
    // verify the required parameter 'endOffset' is set
    if (endOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'endOffset' when calling getUserPhotos");
    }
    
    // create path and map variables
    String localVarPath = "/profile/getUserPhotos".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestedUserId", requestedUserId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "startOffset", startOffset));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "endOffset", endOffset));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<PhotoDetail>> localVarReturnType = new GenericType<List<PhotoDetail>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getUserProfile
   * 
   * @param userId caller user Id (required)
   * @param userToken caller user&#39;s secret token (required)
   * @param requestedUserId user Id to be fetched (required)
   * @return UserProfile
   * @throws ApiException if fails to make API call
   */
  public UserProfile getUserProfile(String userId, String userToken, String requestedUserId) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getUserProfile");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getUserProfile");
    }
    
    // verify the required parameter 'requestedUserId' is set
    if (requestedUserId == null) {
      throw new ApiException(400, "Missing the required parameter 'requestedUserId' when calling getUserProfile");
    }
    
    // create path and map variables
    String localVarPath = "/profile/getUserProfile".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestedUserId", requestedUserId));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<UserProfile> localVarReturnType = new GenericType<UserProfile>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
  /**
   * getUserTrips
   * 
   * @param userId caller user Id (required)
   * @param userToken caller user&#39;s secret token (required)
   * @param requestedUserId user Id to be fetched (required)
   * @param startOffset page start offset (required)
   * @param endOffset page end offset (required)
   * @return List<TripSummary>
   * @throws ApiException if fails to make API call
   */
  public List<TripSummary> getUserTrips(String userId, String userToken, String requestedUserId, Long startOffset, Long endOffset) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'userId' is set
    if (userId == null) {
      throw new ApiException(400, "Missing the required parameter 'userId' when calling getUserTrips");
    }
    
    // verify the required parameter 'userToken' is set
    if (userToken == null) {
      throw new ApiException(400, "Missing the required parameter 'userToken' when calling getUserTrips");
    }
    
    // verify the required parameter 'requestedUserId' is set
    if (requestedUserId == null) {
      throw new ApiException(400, "Missing the required parameter 'requestedUserId' when calling getUserTrips");
    }
    
    // verify the required parameter 'startOffset' is set
    if (startOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'startOffset' when calling getUserTrips");
    }
    
    // verify the required parameter 'endOffset' is set
    if (endOffset == null) {
      throw new ApiException(400, "Missing the required parameter 'endOffset' when calling getUserTrips");
    }
    
    // create path and map variables
    String localVarPath = "/profile/getUserTrips".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userId", userId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "userToken", userToken));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestedUserId", requestedUserId));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "startOffset", startOffset));
    
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "endOffset", endOffset));
    

    

    

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    
    GenericType<List<TripSummary>> localVarReturnType = new GenericType<List<TripSummary>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
    
  }
  
}
