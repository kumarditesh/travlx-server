package com.travlx.client;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.FeedcontrollerApi;
import io.swagger.client.api.LogincontrollerApi;

/**
 * Created by ditesh on 14/4/16.
 */
public class MyClientTest {
  public static void main(String[] args) throws ApiException {
    ApiClient client = new ApiClient();
    client.setBasePath("http://localhost:8080");
    LogincontrollerApi api = new LogincontrollerApi(client);
    FeedcontrollerApi fapi = new FeedcontrollerApi(client);
    System.out.println(api.getOtp("123","1231312312"));
    System.out.println(fapi.getMyFeedMaxOffset("asd","asd"));
  }
}
