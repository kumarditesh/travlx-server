#!/bin/sh
wget http://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.1.6/swagger-codegen-cli-2.1.6.jar -O swagger-codegen-cli.jar
java -jar swagger-codegen-cli.jar generate -i http://localhost:8080/v2/api-docs?group=login -l java -o client
rm -rf ../client/src/main/java/io/swagger/client/*
cp -r ./client/src/main/java/io/swagger/client/* ../client/src/main/java/io/swagger/client/
rm swagger-codegen-cli.jar 
rm -rf client